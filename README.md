# obs-remote-mqtt

MQTT remote control for OBS

Works on linux only, not tested on Mac and Windows. Should be easy to port...

## Dependency

These scripts rely on an external command to send mqtt commands: `mosquitto_sub` and `mosquitto_pub` (from mqtt server mosquitto).

It also rely on command `killall` to stop `mosquitto_sub`.

## Howto use

Install the two scripts `remote-mqtt-in.lua` and `remote-mqtt-out.lua` as scripts in OBS Studio.

For each script, set the parameters to a common mqtt server and a common topic start (make sure it is unique if you use a public mqtt server, such as broker.hivemq.com ). By the way, there is no security. Anyone who knows the topic start can control your OBS. Use at your own risk.

### mqtt commands

Assuming you use the topic start "obs"...
Here are the mqtt messages.

Here are all the message provided to inform about OBS.

| Topic            | Message    | Description  |
| ---------------- | ---------- | ------- |
| obs/active       | intro | current scene is intro  |
| obs/state        | x   | state is x, where x is one of ---, recording, paused, stopping  |
| obs/record       | 1 / 0 | currently recording/not recording |
| obs/pause        | 1 / 0 | currently paused/not paused  |
| obs/scene/intro  | 1     | scene intro is curently active  |
| obs/scene/intro  | 0     | scene intro is currently inactive |

Here are the message which can be used to control OBS.

| Topic            | Message    | Description  |
| ---------------- | ---------- | ------- |
| obs/record       | 1 / 0 | Start / Stop recording |
| obs/pause        | 1 / 0 | Pause / Unpause recording |
| obs/scene/intro  | 1     | Set scene intro as current scene |


## Controlling from Mqtt app on Android

It is easy to monitor and control OBS from a cell phone running an MQTT app.

My suggestion is MQTT Dash.

![MQTT DASH obs remote](img/mqtt-dash-obs-remote.jpg "Mqtt Dash Obs remote")




