--[[
 OBS Lua script
 Send MQTT status information for display state of OBS

 topic        message
 -----        -------
 obs/active   intro      -> current scene is intro
 obs/state    x          -> state is x, where x is one of ---, recording, paused, stopping
 obs/record   1/0        -> currently recording/not recording
 obs/pause    1/0        -> currently paused/not paused

 obs/scene/intro   1     -> scene intro is curently active
 obs/scene/ecran   0     -> scene ecran is currently inactive


--]]

obs           = obslua
source_name   = ""


mqttScene=nil
mqttObs=nil
mqttRecord=nil
mqttPause=nil

local function send_mqtt(topic,message)
	if host then
	    local k=os.execute("mosquitto_pub -q 1 -h "..host.." -t '"..(key or "obs").."/"..topic.."' -m '"..message.."' >/dev/null 2>&1 &")
		if debug then print("message sent "..k) end
	end
end


-- A function named script_description returns the description shown to
-- the user
function script_description()
	return "MQTT remote control. Status only."
end

-- A function named script_properties defines the properties that the user
-- can change for the entire script module itself
function script_properties()
	local props = obs.obs_properties_create()
    obs.obs_properties_add_text(props, "host", "MQTT Host", obs.OBS_TEXT_DEFAULT)
    obs.obs_properties_add_text(props, "topic", "Topic start", obs.OBS_TEXT_DEFAULT)
	obs.obs_properties_add_bool(props, "debug", "Debug")
    return props
end

-- A function named script_defaults will be called to set the default settings
function script_defaults(settings)
    obs.obs_data_set_default_string(settings, "host", "localhost")
    obs.obs_data_set_default_string(settings, "topic", "obs")
	obs.obs_data_set_default_bool(settings, "debug", false)
end

-- A function named script_update will be called when settings are changed
function script_update(settings)
	host = obs.obs_data_get_string(settings, "host")
	key = obs.obs_data_get_string(settings, "topic")
	debug = obs.obs_data_get_bool(settings, "debug")
	send_mqtt("state","---")
end



-- A function named script_save will be called when the script is saved
--
-- NOTE: This function is usually used for saving extra data (such as in this
-- case, a hotkey's save data).  Settings set via the properties are saved
-- automatically.
--function script_save(settings) end

function on_event(event)
	if event==obs.OBS_FRONTEND_EVENT_SCENE_CHANGED
	or event==obs.OBS_FRONTEND_EVENT_SCENE_LIST_CHANGED
	then
			local current = obs.obs_frontend_get_current_scene()
			local currentName = obs.obs_source_get_name(current);
			obs.obs_source_release(current);
			local scenes = obs.obs_frontend_get_scenes()
			for _, scene in ipairs(scenes) do
				local name = obs.obs_source_get_name(scene);
				if name==currentName then
					send_mqtt("active",name)
					send_mqtt("scene/"..name,"1")
				else
					send_mqtt("scene/"..name,"0")
                end
			end
			obs.source_list_release(scenes)
	elseif event==obs.OBS_FRONTEND_EVENT_RECORDING_STARTED then
			send_mqtt("state","recording")
			send_mqtt("record","1");
	elseif event==obs.OBS_FRONTEND_EVENT_RECORDING_UNPAUSED then
			send_mqtt("state","recording")
			send_mqtt("record","1")
			send_mqtt("pause","0")
	elseif event==obs.OBS_FRONTEND_EVENT_RECORDING_PAUSED then
			send_mqtt("state","paused")
			send_mqtt("pause","1")
	elseif event==obs.OBS_FRONTEND_EVENT_RECORDING_STOPPING then
			send_mqtt("state","stopping")
			send_mqtt("record","0")
			send_mqtt("pause","0")
	elseif event==obs.OBS_FRONTEND_EVENT_RECORDING_STOPPED then
			send_mqtt("state","---")
			send_mqtt("record","0")
	--else print("frontend event "..event)
	end
end

-- a function named script_load will be called on startup
function script_load(settings)
	obs.obs_frontend_add_event_callback(on_event)

end
