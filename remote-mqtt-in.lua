--[[
	OBS Lua script
	Monitor a mosquitto incoming messages
	through a text file
	Allows remote control of OBS

	( in what follows, ... is the prefix from parameters and defaults to obs )

    .../scene/set/intro     -> 1/0 is activate/deactivate  scene to intro

	Always use .../scene/set/intro 1   to activate a scene
	You do not need to deactivate scenes explicitely.

    .../record/set     -> 1/0 is start/stop recording
    .../pause/set      -> 1/0 is pause/unpause recording

	.../state   -> provide current state: "---","recording","paused","stopping"
	.../active        -> message is current active scene


--]]


local obs = obslua
local debug -- OBS settings
local activeId = 0 -- active timer id
local current = {} -- current values to compare with text file


-- called when an update to the text file is detected
-- put your code in here to update scenes, etc...
local function update(k, v)
	if debug then obs.script_log(obs.LOG_INFO, string.format("%s has changed to %s", k, v)) end
end

local file=nil
local mosquitto=nil
local fname=nil

-- .../scene/set/intro 1
-- .../record/set 1/0 -> recording
-- .../pause/set 1/0 -> recording

local function process(ligne)
		local from,to = ligne:find("%s+[^%s]+$")
		if not from then return end

		local topic=ligne:sub(1,from-1)

		from,to = ligne:find("[^%s]+$") -- seulement le message
		local msg=ligne:sub(from,to)
		from,to = topic:find("scene/set/")
		if from then
			local sname=topic:sub(to+1)
			if msg=="1" then
				local scenes = obs.obs_frontend_get_scenes()
				if scenes ~= nil then
					for _, scene in ipairs(scenes) do
						local name = obs.obs_source_get_name(scene);
						if name == sname then
								print("set scene "..name)
								obs.obs_frontend_set_current_scene(scene)
						end
					end
				end
				obs.source_list_release(scenes)
			else
				-- on a click sur la scene courante.... aucun changement
			end
		end

		from,to = topic:find("record/set")
		if from then
				local a = obs.obs_frontend_recording_active()
				print("record "..msg.." active="..tostring(a));
				if msg=="1" and not a then
						obs.obs_frontend_recording_start()
				elseif msg=="0" and a then
						obs.obs_frontend_recording_stop()
				end
		end

		from,to = topic:find("pause/set")
		if from then
				local a = obs.obs_frontend_recording_active()
				local p = obs.obs_frontend_recording_paused()
				print("pause "..msg.." recording "..tostring(a).."  paused "..tostring(p));
				if not a then return end
				if msg=="1" and not p then
						obs.obs_frontend_recording_pause(true)
				elseif msg=="0" and p then
						obs.obs_frontend_recording_pause(false)
				end
		end
end

local function checkInput(id)
		if not file then
			fname=os.tmpname()
			if debug then obs.script_log(obs.LOG_INFO, "Using file : "..fname, err) end

		    local err
			file,err = io.open(fname, "rb")
			if not file then
					if debug then obs.script_log(obs.LOG_INFO, string.format("Error reading text file : ", err)) end
					return
			end
			--mosquitto=io.popen("mosquitto_sub -q 1 -h localhost -t '#' -v -u u"..activeId.." >"..fname.." &", "r")
			mosquitto=os.execute("mosquitto_sub -q 1 -h "..host.." -t '"..(key or "obs").."/#" .."' -v  >"..fname.." &")
			--print("got "..tostring(mosquitto))
		end

		local pos=file:seek("cur",0)
		local size=file:seek("end",0)
		file:seek("set",pos)
		--obs.script_log(obs.LOG_INFO,"position is "..pos.. " end is "..size)

		while pos ~= size do
				local ligne=file:read("*l")
				--obs.script_log(obs.LOG_INFO,"got "..ligne)
				process(ligne)
				pos=file:seek("cur",0)
				--obs.script_log(obs.LOG_INFO,"new pos is "..pos.. " end is "..size)
		end

end


local function init()
	-- increase the timer id - old timers will be cancelled
	activeId = activeId + 1

	-- start the timer to check the text file
	-- interval set to 250
	local id = activeId
	obs.timer_add(function() checkInput(id) end, 250)
	obs.script_log(obs.LOG_INFO, string.format("MQTT file monitor started"))
end


----------------------------------------------------------


-- called on startup
function script_load(settings)
		uninit()
end


-- called on unload
function uninit()
		if mosquitto then
				print("killing mosquitto")
				os.execute("killall mosquitto_sub")
				mosquitto=nil
		end
		if fname then
				os.remove(fname)
				fname=nil
		end
		file=nil
end

function script_unload()
		uninit()
end




-- return description shown to user
function script_description()
	return "MQTT remote control. Control only."
end


-- define properties that user can change
function script_properties()
	local props = obs.obs_properties_create()
	obs.obs_properties_add_text(props, "host", "MQTT Host", obs.OBS_TEXT_DEFAULT)
	obs.obs_properties_add_text(props, "topic", "Topic start", obs.OBS_TEXT_DEFAULT)
	obs.obs_properties_add_bool(props, "debug", "Debug")
	return props
end


-- set default values
function script_defaults(settings)
	obs.obs_data_set_default_string(settings, "host", "localhost")
	obs.obs_data_set_default_string(settings, "topic", "obs")
	obs.obs_data_set_default_bool(settings, "debug", false)
end

-- called when settings changed
function script_update(settings)
	host = obs.obs_data_get_string(settings, "host")
	key = obs.obs_data_get_string(settings, "topic")
	debug = obs.obs_data_get_bool(settings, "debug")
	uninit()
	init()
end


-- save additional data not set by user
function script_save(settings)
end
